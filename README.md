# D-Day

[[_TOC_]]

[![The counters](.imgs/historical.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/historical.pdf?job=dist)

This is my remake of the wargame _D-Day_.  The original game
was published by Avalon Hill Game Company in 1964.  This remake is
based on the third edition of the game from 1977. 

Also check out its sister game:
[Afrika Korps](https://gitlab.com/wargames_tex/afrikakorps_tex/). 

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | WWII               |
| Level        | Operational        |
| Hex scale    | ?                  |
| Unit scale   | division (xx)      |
| Turn scale   | 1 week             |
| Unit density | High               |
| # of turns   | 50                 |
| Complexity   | 2 of 10            |
| Solitaire    | 7 of 10            |

Features:
- Campaign
- Tactical air
- Amphibious landing
- Logistics


## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules.

## The board 

The board is quite big (60.8cm tall and 73.5cm wide), and does not
fit into readily available printers.  The board is therefore presented
as a 6-part document.  There are two version: 

- One designed to be printed on A3 paper ([splitboardA3.pdf][]).  This
  holds a relatively comfortable size of counters.
  
- One designed to be printed on A4 paper ([splitboardA4.pdf][]).  This
  is further split into to two, resulting in 12 parts. 
  
(for US Tabloid and Letter, replace `A3` with `Tabloid` and `A4` with
`Letter`). 
  
Print your PDF of choice onto 6 or 12 separate pieces of paper (if you
have a duplex printer, simply print the document) and glue on to
sturdy cardboard (I like 1 1/2mm poster cardboard).  Cut along dashed
crop lines.  You can hold the 6 or 12 pieces together with paper
clamps or the like.

Alternatively you can glue the 6 or 12 sheets of paper together.  Cut
one end along the crop marks (dashed lines) and glue on to the next
part, taking care to align the crop mark arrows.  When playing the
game, put a transparent heavy plastic sheet on top to hold the board
down.

Remember to use the appropriate `materialsX.pdf` file for your board. 

| Board<br>Paper | Board<br>file | Materials<br>file | Materials<br>paper  |
|---------|---------------------------|--------------------------|--------|
| A3      | [splitboardA3.pdf][]      | [materialsA4.pdf][]      | A4     |
| A4      | [splitboardA4.pdf][]      | [materialsA4.pdf][]      | A4     |
| Tabloid | [splitboardTabloid.pdf][] | [materialsLetter.pdf][]  | Letter |
| Letter  | [splitboardLetter.pdf][]  | [materialsLetter.pdf][]  | Letter |

If an A3 print is not available, and the 10 part board is
unmanageable, then one can scale down [splitboardA3.pdf][] by a factor
of $`1/\sqrt{2}=0.7071`$ to get a document that can be printed on an
A4 printer.  In that case, one should _also_ scale down
[materialsA4.pdf][] by the same factor and use the counters and OOB
form that scaled down copy.

To scale down for Letter paper, one should apply a scale factor of
$`0.65`$ to both [splitboardTabloid.pdf][] and
[materialsLetter.pdf][]. 

## The files 

The distribution consists of the following files 

- [DDayA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into 12 parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
  Note that the PDF is a little heavy to load (due to the large map),
  so a bit of patience is needed. 
  
- [DDayA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge, and stabled to form an 24-page A5
  booklet of the rules.

- [DDayA3Booklet.pdf][] Same as `DDayA4Booklet.pdf` above, except
  formatted to be printed on duplex A3. 

- [splitboardA3.pdf][] holds the board in 6 sheets of A3 paper.  Print
  these and glue on to a sturdy piece of cardboard.  You can perhaps
  make some clever arrangement that allows you to unfold the map.
  Otherwise, use paper clambs or the like to hold the board pieces
  together.  
  
- [splitboardA4.pdf][] is like [splitboardA3.pdf][] above, but further
  split to fit on 12 sheets of A4 paper. 
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to   a
  relatively thick piece of cardboard (1.5mm or so) or the like. 
  
- [board.pdf][] Is the whole board as a PDF.  If you have a way of
  printing such a large file, you may find this useful. 

If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*           | *Letter Series*            |
| ----------------------|----------------------------|
| [DDayA4.pdf][]        | [DDayLetter.pdf][]  	     |
| [DDayA4Booklet.pdf][] | [DDayLetterBooklet.pdf][]  |
| [DDayA3Booklet.pdf][] | [DDayTabloidBooklet.pdf][] |
| [materialsA4.pdf][]	| [materialsLetter.pdf][]	 |
| [splitboardA4.pdf][]	| [splitboardLetter.pdf][]	 |
| [splitboardA3.pdf][]	| [splitboardTabloid.pdf][]	 |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## "Old-school" version 

This version mimics the original graphics.  That is, it uses NATO
_friendly_ and _hostile_ colours for Allied and German units,
respectively,  orange and red filled hexes for fortifications and
mountains, respectively, and hatched patterns for towns and
fortresses.  Otherwise, it is the same as above. 

| *A4 Series*               | *Letter Series*              |
| --------------------------|------------------------------|
| [DDayA4OS.pdf][]          | [DDayLetterOS.pdf][]  	   |
| [DDayA4BookletOS.pdf][]   | [DDayLetterBookletOS.pdf][]  |
| [DDayA3BookletOS.pdf][]   | [DDayTabloidBookletOS.pdf][] |
| [materialsA4OS.pdf][]	    | [materialsLetterOS.pdf][]	   |
| [splitboardA4OS.pdf][]	| [splitboardLetterOS.pdf][]   |
| [splitboardA3OS.pdf][]	| [splitboardTabloidOS.pdf][]  |


## VASSAL module 

A [VASSAL](https://vassalengine.org) module of the game is available
in two versions 

- [DDay.vmod][] Regular version 
- [DDayOS.vmod][] "Old-school" version 

The modules are the same, except for the graphics used.  The modules
implements some automatic features, such as step loss of out-of-supply
units, and so on. 

## Previews 

[![The board](.imgs/board.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/splitboardA3.pdf?job=dist "The board")
[![The rules](.imgs/front.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/DDayA4.pdf?job=dist)
[![The charts](.imgs/tables.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/materialsA4.pdf?job=dist)
[![The counters](.imgs/counters.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/materialsA4.pdf?job=dist)

### Ol'school version 

[![The board](.imgs/boardOS.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/splitboardA3.pdf?job=dist "The board")
[![The rules](.imgs/frontOS.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/DDayA4.pdf?job=dist)
[![The charts](.imgs/tablesOS.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/materialsA4.pdf?job=dist)
[![The counters](.imgs/countersOS.png)](https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/materialsA4.pdf?job=dist)


## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality documents,
with vector graphics to ensure that everything scales.   Since the
board is quite big, we must use Lua$`\mathrm{\LaTeX}`$ for the
formatting. 


## The General articles 

[All _The General_
issues](https://www.vftt.co.uk/ah_mags.asp?ProdID=PDF_Gen) as scanned
PDFs.

### 1st Edition ('61)

- Knabe, C.F. II, "Plan Red, Plan Red-Face, Chicago Style", [_The General_, **Vol.1**, #1, p=7-10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%201.pdf#page=7), 1964
- Phillips, V., "Plan Red-Phooey", [_The General_, **Vol.1**, #2, p1](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%202.pdf#page=1), 1964
- Knabe, C.F. II, "Credits and Debits on Plan Red", [_The General_, **Vol.1**, #2, p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%202.pdf#page=7), 1964
- Madeja, V., "Cracking the 'Unstoppable D-Day Defense'", [_The General_, **Vol.1**, #3, p3,7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%203.pdf#page=3), 1964
- Madeja, V., "D-Day Reworked", [_The General_, **Vol.1**, #5, p3](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%205.pdf#page=3), 1965
- Hoffman, K., "German Tactics for D-Day", [_The General_, **Vol.2**, #1, p10-11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%201.pdf#page=10), 1965

### 2nd Edition ('65)

- Editor, "New D-Day '65 - Hot off the press", [_The General_, **Vol.2**, #2, p1](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%202.pdf#page=1), 1965
- Wickstorm, C., "D-Day Used in Medical Research", [_The General_, **Vol.2**, #2, p1-2](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%202.pdf#page=1), 1965
- Plumb, L., "Another Look at D-Day", [_The General_, **Vol.2**, #2, p3](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%202.pdf#page=3), 1965
- Plumb , L., "D-Day '65 Re-visited", [_The General_, **Vol.2**, #3, p2](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%203.pdf#page=2), 1965
- Bosseler, T., "Problems and Solutions", [_The General_, **Vol.2**, #3, p4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%203.pdf#page=4), 1965
- Buynoski, M., "A Plan for Amateur von Rundstedts", [_The General_, **Vol.2**, #3, p5-6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%203.pdf#page=5), 1965
- Stephenson, R., "Operation Robert - D-Day ", [_The General_, **Vol.2**, #3, p8-9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%203.pdf#page=8), 1965
- Phelps, G., "Defensive Reserves", [_The General_, **Vol.2**, #4, p4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%204.pdf#page=4), 1965
- Wiskeyman, D., "Tactics and Strategy Part 3", [_The General_, **Vol.2**, #4, p4-5](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%204.pdf#page=4), 1965
- Drewek, D., "Stop the Allies on the Beaches", [_The General_, **Vol.2**, #4, p5-6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%204.pdf#page=5), 1965
- Ploeg, B., "You Can Defend All Areas", [_The General_, **Vol.2**, #4, p10-11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%204.pdf#page=10), 1965
- Branch, B., "D-Day: The Left Hook-Day", [_The General_, **Vol.3**, #1, p6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%201.pdf#page=6), 1966
- Fellows, R. , "The D-Day That Was", [_The General_, **Vol.3**, #1, p7-8](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%201.pdf#page=7), 1966
- Dotson, J., Notes and Comments: 'The D-Day That Was'", [_The General_, **Vol.3**, #2, p3](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%202.pdf#page=), 1966
- Hales, J., "Bombing in D-Day", [_The General_, **Vol.3**, #2, p3-4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%202.pdf#page=3), 1966
- Smith, R., "Maintain the Offensive in D-Day", [_The General_, **Vol.3**, #3, p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%203.pdf#page=11), 1966
- Clothier, M., "Ultimate German Defense", [_The General_, **Vol.3**, #4, p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%204.pdf#page=11), 1966
- Knabe, C.F. II, "Festung Europa", [_The General_, **Vol.3**, #5, p5-6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%205.pdf#page=5), 1967
- Olson, R., "D-Day - Chicago Style", [_The General_, **Vol.3**, #5, p10-11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%205.pdf#page=10), 1967
- Obolensky, M., "The Real Thing: D-Day", [_The General_, **Vol.3**, #6, p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%206.pdf#page=11), 1967
- Kruger, H., "Normandy - The Way It Really Happened", [_The General_, **Vol.4**, #2, p11-12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2004%20No%202.pdf#page=11), 1967
- Rudolph, M., "Allies Kaput", [_The General_, **Vol.4**, #2, p14](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2004%20No%202.pdf#page=14), 1967
- Zintgraff, G., "The Best Invasion Area - D-Day", [_The General_, **Vol.4**, #3, p8-9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2004%20No%203.pdf#page=8), 1967
- Brundage, M., "Wargamer's Clinic", [_The General_, **Vol.5**, #1, p4-5](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%201.pdf#page=4), 1968
- Augenbraun, A., "Strategic Defense of France", [_The General_, **Vol.6**, #1, p6-7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%201.pdf#page=6), 1969
- Augenbraun, A., "German Defense of Normandy", [_The General_, **Vol.6**, #3, p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%203.pdf#page=11), 1969
- Augenbraun, A., "German Defense of Normandy - Part 2", [_The General_, **Vol.6**, #4, p3-4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%204.pdf#page=3), 1969
- Menyhert, L., "Defense of Festung Europa", [_The General_, **Vol.6**, #4, p6-7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%204.pdf#page=6)
- Augenbraun, A., "German Defense of Normandy - Part 3", [_The General_, **Vol.6**, #5, p6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%205.pdf#page=6), 1970
- Phillies, G., "The D-Day Thesis", [_The General_, **Vol.6**, #5, p8](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%205.pdf#page=8), 1970
- Augenbraun, A., "German Defense of Normandy - Part 4", [_The General_, **Vol.6**, #6, p6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%206.pdf#page=6), 1970
- Zinkharn, J., "Defense Plan III", [_The General_, **Vol.6**, #6, p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%206.pdf#page=7), 1970
- Phillies, G., "The D-Day Thesis – II", [_The General_, **Vol.6**, #6, p8-9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%206.pdf#page=8), 1970
- Bomba, T., "On the Defense of France", [_The General_, **Vol.7**, #1, p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%201.pdf#page=7), 1970
- Burge, R., "TAC Air in D-Day", [_The General_, **Vol.7**, #3, p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%203.pdf#page=7), 1979
- Burge, R., "TAC Air in D-Day - Part II", [_The General_, **Vol.7**, #4, p4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%204.pdf#page=4), 1970
- Bomba, T., "D-Day '44 Tourney Version", [_The General_, **Vol.7**, #4, p5](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%204.pdf#page=5), 1970
- Augenbraun, A., "Winning With the Wehrmacht", [_The General_, **Vol.7**, #5, p5](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%205.pdf#page=5), 1971
- Searight, W., "Gamble of Play Safe?", [_The General_, **Vol.7**, #5, p6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%205.pdf#page=6), 1971
- Lockwood, J., "Fortress Europa", [_The General_, **Vol.9**, #4, p12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2009%20No%204.pdf#page=12), 1972
- Gygax, G., Kuntz, E.G.R., "D-Day Defended", [_The General_, **Vol.10**, #1, p8-9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%201.pdf#page=8), 1973
- Beyma, R., "Anatomy of a Defense", [_The General_, **Vol.10**, #4, p8-10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%204.pdf#page=8), 1973
- Garbisch, R., "D-Day - Variation and Play", [_The General_, **Vol.11**, #1, p3-8](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2011%20No%201.pdf#page=3), 1974
- Greenwood, D., "Beach by Beach", [_The General_, **Vol.11**, #1, p9-10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2011%20No%201.pdf#page=9), 1974
- Searight, W., "D-Day Airborne Operations", [_The General_, **Vol.11**, #5, p24-25](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2011%20No%205.pdf#page=24), 1975
- "Question box"
  - [_The General_, **Vol.1**, #1, p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%201.pdf#page=11) 
  - [_The General_, **Vol.1**, #3, p10,12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%203.pdf#page=10)
  - [_The General_, **Vol.1**, #4, p10,12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%204.pdf#page=10)
  - [_The General_, **Vol.2**, #1, p8](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%201.pdf#page=8)
  - [_The General_, **Vol.5**, #1, p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%201.pdf#page=11)
  - [_The General_, **Vol.5**, #4, p12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%204.pdf#page=12)
  - [_The General_, **Vol.6**, #3, p12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2006%20No%203.pdf#page=12)
  - [_The General_, **Vol.7**, #2, p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%202.pdf#page=11)
  - [_The General_, **Vol.7**, #4, p12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%204.pdf#page=12)
  - [_The General_, **Vol.8**, #2, p14](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2008%20No%202.pdf#page=14)
  - [_The General_, **Vol.8**, #4, p14](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2008%20No%204.pdf#page=14)
  - [_The General_, **Vol.9**, #2, p14](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2009%20No%202.pdf#page=14)
  - [_The General_, **Vol.9**, #4, p14](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2009%20No%204.pdf#page=14)
  - [_The General_, **Vol.10**, #2, p22](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%202.pdf#page=22)
  - [_The General_, **Vol.10**, #4, p22](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%204.pdf#page=22)
  - [_The General_, **Vol.12**, #2, p30](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2012%20No%202.pdf#page=30)

### 3rd Edition ('77)

- Davis,  J.S., "D-Day: The Evolution of a Game", [_The General_, **Vol.14**, #6, p3-13](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2014%20No%206.pdf#page=,3), 1978
- Stahler, J., "The Redesign of D-Day", [_The General_, **Vol.15**, #2, p24-25,32](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2015%20No%202.pdf#page=24), 1978
- Beyma, R., "Fortress Europe Revisited - New Tactics for a Not-So-Old Game", [_The General_, **Vol.15**, #5, p18-21](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2015%20No%205.pdf#page=18), 1979
- Stahler, J., "Hit The Beach - Invasion Defenses in D-Day '77", [_The General_, **Vol.18**, #2, p36-39](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%202.pdf#page=36), 1981
- Lutz, J., "Blood on the Beaches - D-Day, The Allied Invasion", [_The General_, **Vol.21**, #1, p43-44](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2021%20No%201.pdf#page=43), 1984
- Howe, D., "Not Just Another Day - An Overview of D-Day", [_The General_, **Vol.24**, #2, p35-37](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2024%20No%202.pdf#page=35), 1987
- Aldred, R., "One Day at a Time - Strategic and Tactical Hints for D-Day", [_The General_, **Vol.27**, #5, p53-55](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2027%20No%205.pdf#page=53), 1991
- "Question box"
  - [_The General_, **Vol.16**, #3, p34](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2016%20No%203.pdf#page=34)
  - [_The General_, **Vol.18**, #3, p46](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%203.pdf#page=46)
  - [_The General_, **Vol.18**, #4, p46](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%204.pdf#page=46)
 

[artifacts.zip]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/download?job=dist

[DDayA4.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/DDayA4.pdf?job=dist
[DDayA4Booklet.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/DDayA4Booklet.pdf?job=dist
[DDayA3Booklet.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A3-master/DDayA3Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/materialsA4.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/splitboardA3.pdf?job=dist
[splitboardA4.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/splitboardA4.pdf?job=dist
[board.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/board.pdf?job=dist
[DDay.vmod]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-A4-master/DDay.vmod?job=dist

[DDayLetter.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-Letter-master/DDayLetter.pdf?job=dist
[DDayLetterBooklet.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-Letter-master/DDayLetterBooklet.pdf?job=dist
[DDayLetterBooklet.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-Letter-master/DDayLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-Letter-master/materialsLetter.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-Letter-master/splitboardTabloid.pdf?job=dist
[splitboardLetter.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/DDay-Letter-master/splitboardLetter.pdf?job=dist

[DDayA4OS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/DDayA4.pdf?job=dist
[DDayA4BookletOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/DDayA4Booklet.pdf?job=dist
[DDayA3BookletOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA3/DDayA3Booklet.pdf?job=dist
[materialsA4OS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/materialsA4.pdf?job=dist
[splitboardA3OS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/splitboardA3.pdf?job=dist
[splitboardA4OS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/splitboardA4.pdf?job=dist
[boardOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/board.pdf?job=dist
[DDayOS.vmod]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolA4/DDay.vmod?job=dits

[DDayLetterOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolLetter/DDayLetter.pdf?job=dist
[DDayLetterBookletOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolLetter/DDayLetterBooklet.pdf?job=dist
[DDayTabloidBookletOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolTabloid/DDayTabloidBooklet.pdf?job=dist
[materialsLetterOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolLetter/materialsLetter.pdf?job=dist
[splitboardTabloidOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolLetter/splitboardTabloid.pdf?job=dist
[splitboardLetterOS.pdf]: https://gitlab.com/wargames_tex/dday_tex/-/jobs/artifacts/master/file/oldschoolLetter/splitboardLetter.pdf?job=dist





