#!/usr/bin/env python
from datetime import date, timedelta

def f(d):
    suf   = {1:'st',2:'nd',3:'rd'}
    year  = d.year
    month = d.strftime('%B')
    name  = d.strftime('%A')
    day   = d.day
    sday  = f'{day}th' if day > 10 and day < 20 else \
        f'{day}{suf.get(day%10,"th")}'
    return f'{name} {sday} of {month}, {year}'
    
d_day = date(1944,6,6)
week  = timedelta(days=7)

for i in range(52):
    print(f'{i+1:2d} {f(d_day+i*week)}')

    
