# -*- Makefile -*-
#
# Specific rules
#
# --- Split board ----------------------------------------------------
splitboardA3.pdf:	splitboardA3.tex $(BOARD)
splitboardA4.pdf:	splitboardA4.tex $(BOARD)
splitboardTabloid.pdf:	splitboardTabloid.tex $(BOARD)
splitboardLetter.pdf:	splitboardLetter.tex $(BOARD)

# --- Export ---------------------------------------------------------
export.json:	export.pdf
	@echo "EXPORT		- redo"
	@if test -f $@ ; then :; else rm -f $< ; $(MAKE) $< ; fi

# --- Distribution ---------------------------------------------------
# Needed variables
#
# - NAME
# - VERSION
# - MUTE
# - TARGETSA4
# - TARGETSLETTER
#
distdirA4::	$(TARGETSA4) README.md
	@echo "DISTDIR 	$(NAME)-A4-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-A4-$(VERSION)
	$(MUTE)cp $^ $(NAME)-A4-$(VERSION)/
	$(MUTE)touch $(NAME)-A4-$(VERSION)/*

distdirA4OS::
	@echo "DISTDIR 	$(NAME)-A4-$(VERSION)+OS"
	$(MUTE)$(MAKE) clean
	$(MUTE)touch .oldschool
	$(MAKE) distdirA4 VERSION:=$(VERSION)+OS \
		VMOD_TITLE:="$(VMOD_TITLE) - Old-school"

distA4::	distdirA4
	@echo "DIST    	$(NAME)-A4-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-A4-$(VERSION).zip $(NAME)-A4-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

distdirLetter::	$(TARGETSLETTER) README.md
	@echo "DISTDIR 	$(NAME)-Letter-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-Letter-$(VERSION)
	$(MUTE)cp $^ $(NAME)-Letter-$(VERSION)/
	$(MUTE)touch $(NAME)-Letter-$(VERSION)/*

distdirLetterOS::
	@echo "DISTDIR 	$(NAME)-Letter-$(VERSION)+OS"
	$(MUTE)if test -f .oldschool ; then : else $(MAKE) clean; fi
	$(MUTE)touch .oldschool
	$(MAKE) distdirLetter VERSION:=$(VERSION)+OS

distLetter::	distdirLetter
	@echo "DIST    	$(NAME)-Letter-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-Letter-$(VERSION).zip $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)

clean::
	@echo "CLEAN"
	$(MUTE)rm -f *~ *.log *.aux *.out *.lot *.lof *.toc *.auxlock
	$(MUTE)rm -f *.synctex* *.pdf *-pdfjam.pdf *.vmod *_out.tex *.json
	$(MUTE)rm -f splitboard*.pdf calcsplit*.pdf .oldschool
	$(MUTE)rm -f *.png *.svg *.vtmp
	$(MUTE)rm -f $(TARGETS) $(TARGETSA4) $(TARGETSLETTER)
	$(MUTE)rm -f wargame.zip

realclean:: 	clean
	@echo "CLEAN ALL"
	$(MUTE)rm -rf cache/*  labels*.tex
	$(MUTE)rm -rf $(NAME)-*-$(VERSION)
	$(MUTE)rm -rf oldschool* __pycache__

distclean::	realclean
	@echo "CLEAN DIST"
	$(MUTE)rm -rf *.zip
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

#
# EOF
#
