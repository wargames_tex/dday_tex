#
#
#
NAME		:= DDay
VERSION		:= 1.1
VMOD_VERSION	:= 1.1.0
VMOD_TITLE	:= D-Day

LATEX		:= lualatex

DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   preface.tex			\
		   rules.tex			\
		   combat.tex			\
		   consider.tex			\
		   materials.tex
DATAFILES	:= board.tex			\
		   hexes.tex			\
		   tables.tex			\
		   units.tex			\
		   wargame.rough.tex		\
		   historical.tex		\
		   units.tex
OTHER		:= box.tex			\
		   calcsplit.tex		\
		   logo.tex			\
		   spine.tex			\
		   splitboard.tex		
STYFILES	:= dd.sty			\
		   commonwg.sty
BOARD		:= board.pdf
BOARDS		:= $(BOARD)			\
		   historical.pdf
ROUGH		:= wargame.rough.pdf
SIGNATURE	:= 24
PAPER		:= --a4paper
TARGETS		:= $(NAME).pdf 			\
		   $(NAME)Booklet.pdf 		\
		   splitboardA4.pdf		\
		   materials.pdf		\
		   board.pdf			\
		   $(NAME).vmod

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   $(NAME)A3Booklet.pdf 	\
		   splitboardA3.pdf		\
		   splitboardA4.pdf		\
		   materialsA4.pdf		\
		   historical.pdf		\
		   board.pdf			\
		   $(NAME).vmod

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   $(NAME)TabloidBooklet.pdf 	\
		   splitboardTabloid.pdf	\
		   splitboardLetter.pdf		\
		   materialsLetter.pdf
SUBMAKE_FLAGS	:= --no-print-directory

include Variables.mk
include Patterns.mk


a4:	$(TARGETSA4)
us:	$(TARGETSLETTER)
letter:	us
vmod:	$(NAME).vmod 
mine:	a4 cover.pdf box.pdf logo.png 

include Rules.mk

clean::
	@echo "CLEAN"
	$(MUTE)rm -f .oldschool
	$(MUTE)rm -f wargame.zip
	$(MUTE)$(MAKE) $(SUBMAKE_FLAGS)  -C dd clean

realclean:: clean
	@echo "CLEAN ALL"
	$(MUTE)rm -rf cache/*  labels*.tex
	$(MUTE)rm -rf __pycache__ $(NAME)-*-$(VERSION)+OS*
	$(MUTE)$(MAKE) -C dd clean 

distclean:: realclean
	@echo "CLEAN DIST"
	$(MUTE)rm -rf *.zip
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

# These generate images that are common for all formats 
board.pdf:		board.tex	$(DATAFILES) 	$(STYFILES) $(ROUGH)
historical.pdf:		historical.tex	$(DATAFILES)    $(STYFILES) board.pdf
$(ROUGH):		wargame.rough.tex
combat.pdf:		combat.tex 	$(STYFILES) board.pdf

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES) splitboardA4.pdf $(BOARDS)
materialsA4.pdf:	materials.tex 	splitboardA4.pdf $(BOARDS)
$(NAME)A3Booklet.pdf:	PAPER=--a3paper

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) splitboardLetter.pdf $(BOARDS)
materialsLetter.pdf:	materials.tex 	splitboardLetter.pdf $(BOARDS)

# Generic dependency

front.pdf:		front.tex       historical.pdf $(STYFILES)
frontA4.pdf:		front.tex       historical.pdf $(STYFILES)
$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
$(NAME)LetterBooklet.pdf:SIGNATURE=28
$(NAME)TabloidBooklet.pdf:PAPER=--paper '{432mm,279mm}'
$(NAME)TabloidBooklet.pdf:SIGNATURE=28

board.png:		CAIROFLAGS=-scale-to 800
historical.png:		CAIROFLAGS=-scale-to 600

%-pdfjam.pdf:%.pdf
	@echo "PDFJAM $*"
	$(MUTE)$(PDFJAM) --landscape --angle 270 \
		--papersize '{73.5cm,61.6cm}' $< $(REDIR)

export.pdf:	export.tex tables.tex hexes.tex $(STYFILES) units.tex

include Docker.mk

docker-prep::
	mktextfm frunmn
	mktextfm frunbn

docker-artifacts:: distdirA4OS distdirLetterOS

.PRECIOUS:	board.pdf
#
# EOF
#

