from wgexport import *

moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Tips</h2>
  <ul>
   <li>
    <p><img src="doc/ReferenceManual/images/TurnTrackerTurnWindow.png"></p>
    Use the <b>Turn tracker</b> in the menu bar (short cuts
    <code>Alt-T</code> to move forward, <code>Alt-Shift-T</code> to
    move backward), to keep track of the turn and phases.  The module
    will take appropriate actions. 
    <ul>
     <li>Before the <b>Allied supply phase</b>, mark all flipped
      Allied units as isolated</li>    
     <li>After the <b>Allited attrition phase</b> phase, eliminate all
      Allied units that are isolated.</li>
     <li>Before the <b>German isolation phase</b>, mark all flipped
      German units as isolated</li>    
     <li>After the <b>German attrition phase</b> phase, eliminate all
      German units that are isolated.</li>
    </ul>
    Note that game turn marker cannot be moved by hand.  Only the turn
    track int erface will allow movement of the game turn marker.
   </li>
   <li><img class="icon" src="german-icon.png"/> Press the
    <i>Wehrmacht</i> button (<code>Ctrl-Shift-G</code>) to load the
    historical (more or less) German setup.</li>
   <li><img class="icon" src="allied-icon.png"/> Press the <i>SHAEF</i>
    button (<code>Ctrl-Shift-A</code>) to load the historical (more or
    less) Allied setup.</li>
   <li>German V1, V2, and submarine pen installations cannot be moved.
    Use the <b>Capture</b> (<code>Ctrl-E</code> or via right-click
    context menu) item to move the installation to the Allied
    replacement point track.  If you did this on mistake, use the
    <b>Restore</b> menu item (<code>Ctrl-R</code>).  </li>
   <li><img src="isolated-icon.png"> The menu button <b>Show unit
    status</b> (<code>Ctrl-S</code>) shows all units divided by supply
    status (flipped or with isolated marker). Use this to quickly find
    units that may need checking. Note that the module cannot by
    itself figure out the supply status of a unit.  This must be
    checked by a player.  However, once you have flipped a unit
    because it is out of supply, then the above turn actions will take
    care of most of it for you.  Note, the Allied faction should flip
    units back face-up in the Allied attrition phase if a unit has
    come back into supply.  Otherwise, it will automatically be
    eliminated.  </li>
   <li>One can add isolated markers manually, but only if the unit is
    face-down. </li>
   <li>Eliminated units will be sent back to the OOB.  Eliminated
     units are automatically flipped to their out-of-supply side. This
     is to mark them as <i>eliminated</i> units and can therefore
     <i>not</i> be taken as reinforcements or replacements until the
     faction has spent the necessary replacement points.
   </li>
  </ul>
  <h2>About this game</h2>
  <p>
   This VASSAL module was created from
   L<sup>A</sup>T<sub>E</sub>X sources of a Print'n'Play version
   of the <i>D-Day</i> game.  That (a PDF) can be
   found at
  </p>
  <center>
   <code>https://gitlab.com/wargames_tex/dday_tex</code></a>
  </center>
  <p>
   where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.
  </p>
  <p>
   The original game was release by the Avalon Hill Game Company.
  <h3>Credits</h3>
  <dl>
   <dt>Original Research and Design:</dt><dd>Charles Roberts</dd>
   <dt>Original Development:</dt><dd>Richard Hamblen, Donald Greenwood</dd>
   <dt>Third Edition Rewrite:</dt><dd>Jim Stahler</dd>
   <dt>Playtesting:</dt>
   <dd>Joel Davis, Robert Medrow, Robert Beyma, Don
    Howard</dd>
   <dt>Typesetting:</dt><dd>Colonial Composition</dd>
   <dt>Printing:</dt><dd>Monarch Services, Inc.</dd>
  </dl>
  <h2>Copyright and license</h2>
  <p>
   This work is &#127279; 2022 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
  </p>
  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>
  <p>
   or send a letter to
  </p>
  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
</body>
</html>'''

# --------------------------------------------------------------------
# Calculate Hex coordinates from string 
def convHex(loc):
    from math import floor 
    ordz       = ord('Z')
    orda       = ord('A')
    dz         = ordz - orda + 1
    acol, arow = (loc[:-2], loc[-2:]) if len(loc) > 2 else (loc[:-1],loc[-1:])
    row        = int(arow)
    col        = (dz if len(acol) > 1 else 0) + ord(acol[0])-orda
    off        = int((col+1)//2)
    row        = row - off
    icol       = col % dz
    tcol       = chr(icol + orda)
    if col >= dz: tcol += tcol
    return f'{tcol}{row}'

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,verbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO

    notSetup = 'NotInitialSetup'
    vmod.addExternalFile('dd/ObliqueHexGridNumbering.class')
    game = build.getGame()
    game.getGlobalProperties()[0]\
        .addProperty(name         = notSetup,
                     initialValue = False,
                     description  = 'Whether we are not at the start')
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))
    
    maps = game.getMaps()
    main = maps['Board']

    # ----------------------------------------------------------------
    # Get the restore global key, set icon and remove the deadmap map,
    # since we will move eliminated units to the OOBs instead.
    restore = maps['DeadMap'].getMassKeys()['Restore']
    restore['icon'] = 'restore-icon.png'
    game.remove(maps['DeadMap'])
    main.append(restore)

    # ----------------------------------------------------------------
    # Set custom icons on some global keys 
    mkeys = main.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    mkeys['Flip']     ['icon'] = 'flip-icon.png'

    # ----------------------------------------------------------------
    # Add global key to set isolated status 
    main.addMassKey(name         = 'Add isolated marker',
                    buttonHotkey = key('I'),
                    hotkey       = key('I'),
                    buttonText   = '', # g['name']
                    icon         = 'isolated-icon.png',
                    reportSingle = True,
                    tooltip      = 'Add isolated marker')

    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]
    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hex']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    hgrid.remove(hnum)
    # Copy the old parameters
    hnattr = {k:v for k,v in hnum.getAttributes().items()}
    # Set new parameters 
    hnattr['stagger']   = False
    hnattr['direction'] = True
    hnattr['hOff']      = -1
    hnattr['vOff']      = -1
    hgrid.addNode('dd.ObliqueHexGridNumbering',**hnattr)
    if verbose: 
        print(f'Old x0,y0 = {hgrid["x0"]},{hgrid["y0"]} -> 87,29')
    hgrid['x0'] = 87
    hgrid['y0'] = 29


    # ----------------------------------------------------------------
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Allied supply',
                     'Allied invasion',
                     'Allied airborne',
                     'Allied movement',
                     'Allied combat',
                     'Allied tactical air',
                     'Allied attrition',
                     'German isolation',
                     'German movement',
                     'German combat',
                     'German attrition']
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    # ----------------------------------------------------------------
    # Define global keys for turn marker
    # 
    # - A key when moving from allied to german
    # - A key when moving from german to allied
    # - A key when entering the german turn so we can eliminate
    #   isolated Allied units
    # - A key when entering the Allied turn so we can mark out-of-supply
    #   Allied units with the isolated marker
    #
    # Below we define the corresponding global keys 
    prototypes = game.getPrototypes()[0]
    alliedSup  = phaseNames[0]
    germanIso  = phaseNames[7]
    elimTwice  = key(NONE,0)+',elimTwice' # key('E',CTRL_SHIFT)
    markTwice  = key(NONE,0)+',markTwice' # key('I',CTRL_SHIFT)
    testInit   = key(NONE,0)+',testInitial' # key
    isInit     = key(NONE,0)+',isInitial'
    turns.addHotkey(hotkey       = markTwice,
                    match        = f'{{Phase=="{alliedSup}"}}',
                    #reportFormat = '--- Mark isolated Allied units ---',
                    reportFormat = '',
                    name         = 'Allied supply')
    turns.addHotkey(hotkey       = markTwice,
                    match        = f'{{Phase=="{germanIso}"}}',
                    #reportFormat = '--- Mark isolated German units ---',
                    reportFormat = '',
                    name         = 'German isolation')
    turns.addHotkey(hotkey       = elimTwice,
                    match        = f'{{Phase=="{germanIso}"}}',
                    #reportFormat = '--- Eliminating twice isolated Allied units ---',
                    reportFormat = '',
                    name         = 'German attrition')
    turns.addHotkey(hotkey       = elimTwice,
                    match        = f'{{Phase=="{alliedSup}"}}',
                    #reportFormat = '--- Eliminating twice isolated German units ---',
                    reportFormat = '',
                    name         = 'German attrition')
    turns.addHotkey(hotkey       = testInit,
                    match        = '{true}',
                    reportFormat = ''
                    #reportFormat = '{"Test initial (turn) "'+\
                    #               '+Turn+"/"+Phase+": "+notSetup}'
                    )
    
    # Add some global keys to the map
    #
    # - Flip turn marker (Ctrl+Shift+F)
    # - Mark twice out-of-supply Allied units (Ctrl+Shift+I)
    # - Eliminate twice isolated Allied units (Ctrl+Shift+E)
    main.addMassKey(name         = 'Flip turn marker',
                    buttonHotkey = key('F',CTRL_SHIFT),
                    hotkey       = key('F',CTRL),
                    buttonText   = '', 
                    target       = '',
                    filter       = ('{BasicName == "game turn"}'))
    #
    main.addMassKey(name         = 'Mark twice isolated Allied units',
                    buttonHotkey = markTwice,
                    hotkey       = key('I',CTRL),
                    buttonText   = '', 
                    target       = '',
                    reportFormat = '',
                    filter       = f'{{Phase=="{alliedSup}"&&'\
                                      'Faction=="Allied"&&Step_Level==2}')
    #
    main.addMassKey(name         = 'Mark twice isolated German units',
                    buttonHotkey = markTwice,
                    hotkey       = key('I',CTRL),
                    buttonText   = '', 
                    target       = '',
                    reportFormat = '',
                    filter       = f'{{Phase=="{germanIso}"&&'\
                                      'Faction=="German"&&Step_Level==2}')
    #
    main.addMassKey(name         = 'Eliminate twice isolated Allied units',
                    buttonHotkey = elimTwice,
                    hotkey       = key('E',CTRL),
                    buttonText   = '', 
                    target       = '',
                    filter       = f'{{Phase=="{germanIso}"&&'\
                                      'Faction=="Allied"&&Isolated_Level==2}')
    #
    main.addMassKey(name         = 'Eliminate twice isolated German units',
                    buttonHotkey = elimTwice,
                    hotkey       = key('E',CTRL),
                    buttonText   = '', 
                    target       = '',
                    filter       = f'{{Phase=="{alliedSup}"&&'\
                                      'Faction=="German"&&Isolated_Level==2}')
    # 
    main.addMassKey(name         = 'German historical setup',
                    icon         = 'german-icon.png',
                    buttonHotkey = key('G',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startHex',
                    buttonText   = '',
                    target       = '',
                    canDisable   = True,
                    propertyGate = notSetup,
                    tooltip      = 'Load (semi-) historical German setup',
                    filter       = '{Faction=="German"}')
    main.addMassKey(name         = 'Allied historical setup',
                    icon         = 'allied-icon.png',
                    buttonHotkey = key('A',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startHex',
                    buttonText   = '',
                    target       = '',
                    canDisable   = True,
                    propertyGate = notSetup,
                    tooltip      = 'Load (semi-) historical Allied setup',
                    filter       = '{Faction=="Allied"}')
    main.addMassKey(name         = 'Test for initial setup',
                    icon         = '',
                    buttonHotkey = testInit,
                    hotkey       = isInit,
                    buttonText   = '',
                    target       = '',
                    tooltip      = 'Test for initial setup',
                    reportFormat = '',
                    #reportFormat = '{"Test initial (mass) "+' + \
                    #               'Turn+"/"+Phase+": "+notSetup}',
                    filter       = '{BasicName == "game turn"}')


    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypes to the game turn marker later on. 
    turnp = []
    for t in range(1,51):
        k = key(NONE,0)+f',Turn{t}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&'
                                        f'Phase=="{alliedSup}"}}'),
                        reportFormat = f'=== Turn {t} ===',
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = ('{BasicName == "game turn"}'))
        pn     = f'To turn {t}'
        traits = [ SendtoTrait(mapName     = 'Board',
                               boardName   = 'Board',
                               name        = '',
                               restoreName = '',
                               restoreKey  = '',
                               zone        = f'Turn track',
                               destination = 'R', #R for region
                               region      = ('game turn' if t == 1 else
                                              f'Turn {t}'),
                               key         = k,
                               x           = 0,
                               y           = 0),
                   BasicTrait()]
        prototypes.addPrototype(name        = f'{pn} prototype',
                                description = f'{pn} prototype',
                                traits      = traits)
        turnp.append(pn)
    turns.addHotkey(hotkey       = key('F',CTRL_SHIFT),
                    match        = f'{{Phase=="{germanIso}"}}',
                    reportFormat = '--- German Turn ---',
                    name         = 'German Turn')
    turns.addHotkey(hotkey       = key('F',CTRL_SHIFT),
                    match        = f'{{Phase=="{alliedSup}"}}',
                    reportFormat = '--- Allied Turn ---',
                    name         = 'Allied Turn')

    
    # ----------------------------------------------------------------
    # Re-define some prototypes.  We used short hands, when making the
    # counters, for unit types.  We simply give better names here.
    # Mainly for the inventory.
    prots = {'i': 'Infantry',
             'a': 'Armoured',
             'g': 'Static',
             'h': 'Headquarters',
             'm': 'Mechanized infantry' }
    protos = prototypes.getPrototypes()
    for n,t in prots.items():
        p = protos.get(f'{n} prototype',None)
        if p is None:
            if verbose:
                print(f'Prototype {n} not found')
            continue

        traits = p.getTraits()
        for trait in traits:
            if trait.ID == MarkTrait.ID:
                trait['value'] = t

        p.setTraits(*traits)

    # ----------------------------------------------------------------
    # Add an inventory sorted on the unit supply and isolation status. 
    filt = ('{(Faction=="Allied"||Faction=="German") && '
            '(Command != "air" && Command != "equipment" && '
            'Command != "other" && Command != "installation")}')
    grp  = 'Faction,Step_Level,Isolated_Level'
    grp  = 'Faction,SupplyState'
    disp = ('{PropertyValue==Faction ? Faction : '
            'SupplyState==3 ? "Twice isolated" : '
            'SupplyState==2 ? "Isolated" : "In supply"}')
    game.addInventory(include       = filt,
                      groupBy       = grp,
                      sortFormat    = '$PieceName$',
                      tooltip       = 'Show supply status of pieces',
                      nonLeafFormat = disp, # '$PropertyValue$',
                      zoomOn        = True,
                      hotkey        = key('S',ALT),
                      refreshHotkey = key('S',ALT_SHIFT),
                      icon          = 'isolated-icon.png')
        

    # ----------------------------------------------------------------
    #
    # Remove 'Eliminate' trait from faction traits.  We will replace
    # that trait on each unit with a trait that moves the unit back to
    # its place on the OOB.
    #
    # We create the traits once, since they will be copied to the prototypes
    # Phases to allow the isolate command
    enbIso   = { 'Allied': alliedSup, 
                 'German': germanIso }
    enbUiso  = {'Allied': phaseNames[6],
                'German': phaseNames[-1] }
    isoExp   = '{Step_Level!=2}'
    isoUexp  = '{Isolated_Level!=2}'
    rstIso   = key(NONE,0)+',resetIsolated'
    rstStep  = key(NONE,0)+',resetStep' 
    tglStep  = key(NONE,0)+',toggleStep'
    elimKey  = key(NONE,0)+',eliminate'
    deiso    = key('I',CTRL_SHIFT)
    restIso  = RestrictCommandsTrait(name='Restrict isolated command',
                                     hideOrDisable = 'Disable',
                                     expression = '',
                                     keys = [key('I')])
    restUiso = RestrictCommandsTrait(name='Restrict de-isolate command',
                                     hideOrDisable = 'Disable',
                                     expression    = '',
                                     keys          = [deiso])
    isolated = LayerTrait(['','isolated-mark.png'],
                          ['','Isolated +'],
                          activateName = '',
                          activateMask = '',
                          activateChar = '',
                          increaseName = 'Isolated',
                          increaseMask = CTRL,
                          increaseChar = 'I',
                          decreaseName = '',
                          decreaseMask = '',
                          decreaseChar = '',
                          resetKey     = rstIso,
                          under        = False,
                          underXoff    = 0,
                          underYoff    = 0,
                          name         = 'Isolated',
                          loop         = False,
                          description  = '(Un)Mark unit as isolated',
                          always       = False,
                          activateKey  = '',
                          increaseKey  = key('I'),
                          decreaseKey  = '',
                          scale        = 1)
    actions = [rstIso,
               rstStep,
               tglStep,
               elimKey]
    elim = TriggerTrait(name       = 'Eliminate unit',
                        command    = 'Eliminate',
                        key        = key('E'),
                        actionKeys = actions)
    flip = TriggerTrait(name       = 'Flip unit',
                        command    = 'Flip',
                        key        = key('F'),
                        actionKeys = [rstIso,tglStep])
    uIso = TriggerTrait(name       = 'De-isolate unit',
                        command    = 'Not isolated',
                        key        = deiso,
                        actionKeys = [rstIso])

    # Now modify the faction prototypes 
    for faction in ['Allied', 'German']:
        proto  = protos[f'{faction} prototype']
        traits = proto.getTraits()
        basic  = traits.pop()
        
        toremove = []
        oldelim  = Trait.findTrait(traits,SendtoTrait.ID,
                                   'name','Eliminate')
        if oldelim is not None: traits.remove(oldelim)

        restIso['expression']  = isoExp
        restUiso['expression'] = isoUexp
        isoPhs                 = enbIso.get(faction,None)
        uisoPhs                = enbUiso.get(faction,None)
        if isoPhs is not None:
            restIso['expression'] = '{'+\
                isoExp[1:-1] + '||CurrentZone!="hex"||' + \
                f'Phase!="{isoPhs}"' + '}'
        if uisoPhs is not None:
            restUiso['expression'] = '{'+\
                isoUexp[1:-1] + '||CurrentZone!="hex"||' + \
                f'Phase!="{uisoPhs}"' + '}'
            
            
        # '&&'.join(f'Phase!="{p}"' for p in isoPhs) + ')}'
        state = CalculatedTrait('SupplyState',
                                '{Step_Level==1 ? 1 : '+\
                                'Isolated_Level == 1 ? 2 : 3}')
                                
        
        traits.extend([flip,restIso,isolated,restUiso,uIso,elim,state,basic])
        proto.setTraits(*traits)

    # Create an extra Allied air prototype
    elim['command'] = 'Return to base'
    prototypes.addPrototype(name        = 'Allied air prototype',
                            traits      = [elim,
                                           MarkTrait('Faction','Allied'),
                                           BasicTrait()],
                            description = 'Prototype of Allied air units')

    # ----------------------------------------------------------------
    # Get all pieces and add Eliminate action that moves the unit to
    # the OOB. This is also where we add the traits to move the game
    # turn marker on the turn track.
    #
    # We also add another piece window to add the Allied supply and
    # Mulberry markers there
    alliedSups = game.addPieceWindow(name    = 'Allied supplies',
                                     hotkey  = key('M',ALT),
                                     tooltip = 'Show/hide Allied markers',
                                     icon    = 'mulberry-icon.png')
    # We remove it, and add it back in before the map
    game.remove(alliedSups)
    game.insertBefore(alliedSups,main)
    # We add a list view
    alliedList = alliedSups.addList(entryName = 'Allied supplies')
    # We have a list of supply names
    supNames = ['bremen','north sea','pas de calais','le havre',
                'normandy'',brittany','bay of biscay','south france',
                'mulberry']
    # Next is a dictionary of historic positions for the Allied units.
    alliedHist = {'us id 4':     'R33', # Utah
                  'us id 9':     'R33',
                  'us id 1':     'R32', # Omaha
                  'br id 50':    'R32', # Gold
                  'br id 3':     'R31', # Sword
                  'ca id 3':     'R31', # Juno
                  'us abid 82':  'T35', # Utah
                  'us abid 101': 'T35', # Utah
                  'br abid 1':   'T33', # Sword
                  'br f 1':      'T32',
                  'us f 1':      'S35' }
    pieces  = game.getPieces(asdict=False)
    germans = ['de ','ss ']
    allies  = ['al ','br ','ca ','fr ','po ','us ']
    enbFlip = {'Allied': alliedSup,
               'German': germanIso }
    for piece in pieces:
        name    = piece['entryName'].strip()
        fac     = name[:3]
        faction = 'German' if fac in germans else 'Allied'
        # German and Allied pieces 
        if fac in germans+allies and not ' rp ' in name:
            traits               = piece.getTraits()
            basic                = traits.pop()
            step                 = Trait.findTrait(traits,
                                                   LayerTrait.ID,
                                                   'name', 'Step')
            if step is not None:
                step['resetKey']     = rstStep
                step['increaseKey']  = tglStep
                step['increaseName'] = ''
            else:
                print('No Step trait found!')
            
            elim   = SendtoTrait(name        = '',
                                 key         = elimKey,
                                 mapName     = 'Board',
                                 boardName   = 'Board',
                                 restoreName = f'Restore {name}',
                                 restoreKey  = key('R'),
                                 destination = 'R',
                                 region      = name)
            traits.append(elim)

            st, hx = None, None
            # German pieces.  Get the start-up hex from the
            # upper right mark 
            if faction == 'German':
                start = Trait.findTrait(traits,MarkTrait.ID,
                                        'name', 'upper left')
                if start is not None:
                    st = start['value']
                    st = sub('[^=]+=','',st).strip()
                    hx = convHex(st)

            # Allied unit.  Check if it is in the list. 
            elif faction == 'Allied':
                if name in alliedHist:
                    st = alliedHist[name]
                    hx = convHex(st)
                if " f " in name or " b " in name: # Fighters or bombers
                    step['increaseName'] = 'Refuel'
                    # Change from Allied prototype to Allied air prototype
                    # We don't want move trail, etc.
                    trait = Trait.findTrait(traits,PrototypeTrait.ID,
                                            'name', 'Allied prototype')
                    if trait is not None:
                        trait['name'] = 'Allied air prototype'

            if st is not None and hx is not None:
                # print(f'  Start hex: {hx}')
                traits.append(
                    SendtoTrait(name        = '',
                                key         = key(NONE,0)+',startHex',
                                mapName     = 'Board',
                                boardName   = 'Board',
                                destination = 'G',
                                restoreName = '',
                                restoreKey  = '',
                                position    = hx))


            phase = enbFlip.get(faction,None)
            if phase is not None:
                traits.insert(0,
                              RestrictCommandsTrait(
                                  'Restrict step command',
                                  hideOrDisable = 'Disable',
                                  expression    = f'{{Phase!="{phase}"}}',
                                  keys          = [key('F')]))
                
            traits.append(basic)
            piece.setTraits(*traits)

        # Add prototype traits to game turn marker.  Note, we
        # remove the basic piece trait from the end of the list,
        # and add it in after adding the other traits: The basic
        # piece trait _must_ be last.
        elif name == 'game turn':
            traits  = piece.getTraits()
            basic   = traits.pop()
            mtrait  = Trait.findTrait(traits,PrototypeTrait.ID,
                                       'name','Markers prototype')
            if mtrait is not None:
                traits.remove(mtrait)

            rtrait  = Trait.findTrait(traits,ReportTrait.ID)
            if rtrait is not None:
                traits.remove(rtrait)
                
            traits.extend([PrototypeTrait(pnn + ' prototype')
                           for pnn in turnp])
            nomove = RestrictAccessTrait(sides=[],
                                         description='Cannot move')
            prop   = GlobalPropertyTrait(
                ['Is initial',isInit,GlobalPropertyTrait.DIRECT,
                 f'{{Turn!=1||Phase!="{alliedSup}"}}'
                 ],
                name = notSetup)
            traits.extend([prop,nomove])
            traits.append(basic)
            piece.setTraits(*traits)

        # RP marker
        elif ' rp ' in name:
            traits = piece.getTraits()
            basic  = traits.pop()
            mark   = MarkTrait('Command','other')
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     'name', 'Step')
            if step is not None:
                traits.remove(step)
            
            traits.extend([mark,basic])
            piece.setTraits(*traits)

        # Check if we have an Allied supply marker 
        else:
            if not any([name.startswith(sup) for sup in supNames]):
                continue
            traits = piece.getTraits()
            basic  = traits.pop()
            ftrait = Trait.findTrait(traits,PrototypeTrait.ID,
                                     'name','Allied prototype')
            traits.remove(ftrait)
            traits.extend([MarkTrait('Faction','Allied'),
                           DeleteTrait('Delete',key('D')),
                           basic])
            piece.setTraits(*traits)
            alliedList.addPiece(piece)

    # ----------------------------------------------------------------
    # German installations and replamement points on board.  This
    # should be done last so that we get  the changes above in the
    # cloned pieces.  We also redefine the traits. 
    places = {'N24':         ['v1'], # Really N24
              'H14':         ['v2'], # Really N14
              'V43':         ['sub'], # Really V43
              'Allied RP 0': ['al rp 1', 'al rp 10' ],
              'German RP 0': ['de rp 1', 'de rp 10' ]}
    for place, pnames in places.items():
        pieces  = game.getSpecificPieces(*pnames)
        for p in pieces:
            name      = p['entryName'].strip()
            traits    = p.getTraits()
            basic     = traits.pop()
            fac       = name[:3]
            faction   = 'Allied' if fac in allies else 'German'
            ftrait    = Trait.findTrait(traits,PrototypeTrait.ID,
                                        'name', f'{faction} prototype')
            traits.remove(ftrait)
            traits.extend([MarkTrait(name='Faction',value=faction)])
            if 'RP' not in place:
                place = convHex(place)
                traits.extend([SendtoTrait(mapName     = 'Board',
                                           boardName   = 'Board',
                                           name        = 'Capture',
                                           key         = key('E'),
                                           restoreName = 'Restore',
                                           restoreKey  = key('R'),
                                           destination = 'G',
                                           position    = '-B24'), # -B24
                               ReportTrait(key('E'),
                                           report = '$location$: $newPieceName$ $menuCommand$ by Allied forces *'),
                               RestrictAccessTrait(sides=[],
                                                   description='Cannot move')])
            
            traits.append(basic)
            p.setTraits(*traits)
        
        atstart = main.addAtStart(name        = place.capitalize(),
                                  location    = place,
                                  owningBoard = 'Board')
        atstart.addPieces(*pieces)

        

            
#
# EOF
#

                      
    
    
